#!/bin/bash

logfile=/home/pi/needforspeed/$HOSTNAME/speedlog.txt

date >> $logfile
speedtest | grep -e 'Download' -e 'Upload' >> $logfile
printf "\n" >> $logfile

git add -a
git commit -m"$HOSTNAME " 
git push
